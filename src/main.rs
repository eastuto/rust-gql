#[macro_use] extern crate juniper;
extern crate juniper_iron;
extern crate iron;
extern crate hyper;
extern crate mount;
extern crate rustc_serialize;

use mount::Mount;
use iron::prelude::*;
use juniper::EmptyMutation;
use juniper_iron::GraphQLHandler;
use hyper::Client;
use std::io::Read;
use rustc_serialize::json;

fn context_factory(_: &mut Request) -> IronResult<()> {
    Ok(())
}

#[derive(RustcEncodable, RustcDecodable)]
pub struct Listing {
    pub id: i32,
    pub address: String,
    pub agent_id: i32,
}

#[derive(RustcEncodable, RustcDecodable)]
pub struct Agent {
    pub id: i32,
    pub user_id: i32,
}

#[derive(GraphQLObject)]
#[derive(RustcEncodable, RustcDecodable)]
pub struct User {
    id: i32,
    first_name: String,
    second_name: String,
    email: String,
    active: bool
}

graphql_object!(Listing: () |&self| {
    field id() -> i32 {
        self.id
    }

    field address() -> &str {
        self.address.as_str()
    }

    field agent() -> Agent {
        let client = Client::new();
        let uri = format!("http://127.0.0.1:7878/api/agent/{}", self.id);
        let mut res = client.get(&uri).send().unwrap();
        assert_eq!(res.status, hyper::Ok);
        let mut s = String::new();
        res.read_to_string(&mut s).unwrap();
        let agent = json::decode(&s).unwrap();
        agent
    }
});

graphql_object!(Agent: () |&self| {
    field id() -> i32 {
        self.id
    }

    field user_id() -> i32 {
        self.user_id
    }

    field user() -> User {
        let client = Client::new();
        let uri = format!("http://127.0.0.1:7878/api/user/{}", self.id);
        let mut res = client.get(&uri).send().unwrap();
        assert_eq!(res.status, hyper::Ok);
        let mut s = String::new();
        res.read_to_string(&mut s).unwrap();
        let user = json::decode(&s).unwrap();
        user
    }
});

struct Root;

graphql_object!(Root: () |&self| {
    field listing(id: i32) -> Listing {
        let client = Client::new();
        let uri = format!("http://127.0.0.1:7878/api/listings/{}", id);
        let mut res = client.get(&uri).send().unwrap();
        assert_eq!(res.status, hyper::Ok);
        let mut s = String::new();
        res.read_to_string(&mut s).unwrap();
        let listing = json::decode(&s).unwrap();
        listing
    }
});

fn main() {
    let mut mount = Mount::new();
    let graphql_endpoint = GraphQLHandler::new(
        context_factory,
        Root,
        EmptyMutation::<()>::new(),
    );
    mount.mount("/graphql", graphql_endpoint);
    let chain = Chain::new(mount);
    Iron::new(chain).http("127.0.0.1:8080").unwrap();
}
