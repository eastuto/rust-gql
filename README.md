# Introduction

A GraphQL aggregator service built in Rust.  

# Getting started

## Dependencies

You will need:

### Rust

Install rust - https://www.rust-lang.org/tools/install.

### The Rest service 

This thing will connect to a REST service so you will need to clone and set that up first.

# Start project 

```cargo run ```

Try a gql query

Either with a GraphQL browser extension (like Altair on Firefox)

```
query{listing(id:1) { 
     address
     agent {
       user {
         firstName
         secondName
       }
     }
   	}
   }
```

Or via http client 

```http://127.0.0.1:8080/graphql?query=query{listing(id:1) { address } }```


# Questions ? ( ͡◉◞ ͜ʖ◟ ͡◉)

Email Eugene Astuto eastuto@gmail.com

